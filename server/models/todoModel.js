const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const todoSchema = new Schema({
  task: {
    type: String,
    required: true,
    unique: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
});
module.exports = mongoose.model("Todos", todoSchema);
