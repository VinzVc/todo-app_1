const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/todoControllers");

router.get("/", controller.findAll);

router.post("/add", controller.add);

router.post("/complete", controller.complete);

router.post("/delete", controller.delete);

module.exports = router;
