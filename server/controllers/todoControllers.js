const Todos = require("../models/todoModel");

class TodoController {
  async findAll(req, res) {
    try {
      res.send(await Todos.find({}));
    } catch (e) {
      res.send(e);
    }
  }

  async add(req, res) {
    try {
      res.send(await Todos.create({ task: req.body.task, completed: false }));
    } catch (e) {
      res.send(e);
    }
  }

  async complete(req, res) {
    try {
      res.send(
        await Todos.updateOne(
          { _id: req.body.id },
          { $set: { completed: true } }
        )
      );
    } catch (e) {
      res.send(e);
    }
  }

  async delete(req, res) {
    try {
      res.send(await Todos.deleteOne({ _id: req.body.id }));
    } catch (e) {
      res.send(e);
    }
  }
}

module.exports = new TodoController();
