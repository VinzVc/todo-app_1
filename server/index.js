const mongoose = require("mongoose"),
  cors = require("cors"),
  express = require("express"),
  app = express(),
  todoRoutes = require("./routes/todoRoutes"),
  path = require("path"),
  port = process.env.PORT || 3030;
require("dotenv").config();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

async function connectToDB() {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.DB_user}:${process.env.DB_pw}@todo-app.qjy8y.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      }
    );
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}

app.use("/todos", todoRoutes);
app.get("/", (req, res) => {
  res.send("Home Page");
});

connectToDB();
app.listen(port, () => console.log(`🚀 Serving on port ${port} 🚀`));
