import axios from "axios";
import React from "react";
import { URL } from "./config";

export default function ToDoList(props) {
  const { list, getList } = props;

  let completeToDo = async (e) => {
    const taskId = e.target.getAttribute("taskid");
    await axios
      .post(`${URL}/todos/complete`, { id: taskId, completed: true })
      .catch((err) => console.log(err));
    getList();
  };

  let deleteToDo = async (e) => {
    const taskId = e.target.getAttribute("taskid");
    await axios
      .post(`${URL}/todos/delete`, { id: taskId })
      .catch((err) => console.log(err));
    getList();
  };

  let renderList = () =>
    list.map((elm, idx) => {
      return (
        <div key={idx} className="task">
          <li
            style={{ textDecoration: elm.completed ? "line-through" : "none" }}
          >
            {elm.task}
          </li>
          <div className="taskActions">
            <input
              type="image"
              alt="check"
              taskid={elm._id}
              src="/checked-svgrepo-com.svg"
              onClick={completeToDo}
            />
            <input
              type="image"
              alt="cancel"
              taskid={elm._id}
              src="/cancel-svgrepo-com.svg"
              onClick={deleteToDo}
            />
          </div>
        </div>
      );
    });

  return <div>{renderList()}</div>;
}
