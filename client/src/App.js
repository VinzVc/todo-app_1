import React, { useState, useEffect } from "react";
import { URL } from "./config";
import axios from "axios";
import ToDoList from "./ToDoList";
import "./App.css";

function App() {
  const [userInput, setUserInput] = useState("");
  const [list, setList] = useState([]);

  useEffect(() => {
    getList();
  }, []);

  let getList = () => {
    axios
      .get(`${URL}/todos`)
      .then((res) => setList(res.data))
      .catch((err) => console.log(err));
  };

  let addToDo = async (e) => {
    e.preventDefault();
    await axios.post(`${URL}/todos/add`, { task: userInput }).then(() => {
      setUserInput("");
      getList();
    });
  };

  return (
    <div className="App">
      <h1>To Do App</h1>
      <form onSubmit={addToDo}>
        <input
          className="todoInput"
          type="text"
          onChange={(e) => setUserInput(e.target.value)}
          value={userInput}
        />
        <input
          className="addButton"
          type="image"
          src="/add-svgrepo-com.svg"
          alt="addButton"
        />
      </form>
      <h2>Tasks</h2>

      <ToDoList getList={getList} list={list} />
    </div>
  );
}

export default App;
