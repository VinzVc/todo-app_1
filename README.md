# ToDo App

My first full stack app : a simple to do app.
You can access it here : http://fs-todo.surge.sh

## Usage

For practicing reason, added tasks go to a single Database. Please clean what you added when you're done using it

## Technologies

Client published with surge, server deployed with heroku.
Technologies used : MERN and pure CSS.
